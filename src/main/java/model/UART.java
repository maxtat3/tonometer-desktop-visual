package model;

import controller.Controller;
import controller.ControllerCallback;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;


// TODO: 01.09.18 renamed to UART

/**
 *
 */
public class UART {

	private static final boolean IS_LOG = false;

	public static final String[] NUMBERS_OF_COM_PORTS = {
		"COM1","COM2","COM3","COM4","COM5","COM6","COM7","COM8",
		"COM9","COM10","COM11","COM12","COM13","COM14","COM15","COM16",
		"/dev/ttyUSB0", "/dev/ttyUSB1", "/dev/ttyUSB2", "/dev/ttyUSB3",
		"/dev/ttyACM0", "/dev/ttyACM1", "/dev/ttyACM2", "/dev/ttyACM3"
	};

	/**
	 * Default COM port class should be applied for UI combobox elements selector.
	 * Value INDEX indicate index of array of all COM port list.
	 *
	 * @see DefaultCOMPort#INDEX
	 * @see UART#NUMBERS_OF_COM_PORTS
	 */
	public static final class DefaultCOMPort {
		private static final int INDEX = 16;
		public static final String NAME = NUMBERS_OF_COM_PORTS[INDEX];
		public static final int NUMBER = INDEX;
	}

	private SerialPort serialPort;

	/**
	 * Callback for Controller.
	 * Using it callback, model must be send data to Controller.
	 *
	 * @see Controller
	 * @see UART
	 */
	private ControllerCallback controllerCallback;

	// Измерение времени между получением каждого нового буфера
	private long timeRXBuffN = 0;
	private long timeRXBuffNp1 = 0;
	private long dTimeBetweenRXBuff = 0;


	public UART() {
	}


	public void addControllerCallback(ControllerCallback cc) {
		this.controllerCallback = cc;
	}

	/**
	 * Possible UART COM port states
	 */
	public enum PortStates {
		OPEN,
		CLOSE
	}


	/**
	 * Checked COM port is open ?
	 *
	 * @return true if port open
	 */
	public boolean isOpen() {
		return serialPort != null && serialPort.isOpened();
	}

	/**
	 * Do close open COM port.
	 *
	 * @return true if port normally closed
	 */
	public boolean close() {
		try {
			return serialPort.closePort();
		} catch (SerialPortException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Write String to opened COM port
	 *
	 * @param buff buffer writes to port
	 */
	public void writeByteBuff(int[] buff) {
		try {
			serialPort.writeIntArray(buff);
		} catch (SerialPortException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Init and open COM port
	 *
	 * @param portName opened port name
	 */
	public void init(String portName){
		serialPort = new SerialPort(portName);
		try {
			serialPort.openPort();
			controllerCallback.setPortState(PortStates.OPEN);
//			serialPort.setParams(SerialPort.BAUDRATE_38400,
			serialPort.setParams(SerialPort.BAUDRATE_115200,
//			serialPort.setParams(SerialPort.BAUDRATE_256000,
				SerialPort.DATABITS_8,
				SerialPort.STOPBITS_1,
				SerialPort.PARITY_NONE);
			//Включаем аппаратное управление потоком (для FT232 нжуно отключать)
//            serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_RTSCTS_IN |
//                                          SerialPort.FLOWCONTROL_RTSCTS_OUT);
			serialPort.addEventListener(new PortReader(), SerialPort.MASK_RXCHAR);
		}
		catch (SerialPortException ex) {
			ex.printStackTrace();
			log("Порт закрыт");
			controllerCallback.setPortState(PortStates.CLOSE);
		}
	}


	public void startedTimeMs(long startMsrMs) {
		timeRXBuffN = startMsrMs;
	}


	private class PortReader implements SerialPortEventListener {
		private long start = 0, end = 0;
		private boolean isFreezeTime = false;	// do not modify

		private int BUFF_SIZE = 128;
//		private int BUFF_SIZE = 256;
		private int POINTS_IN_BUFF = 16;    // количество точек в буфере отображаемых на графике

		private byte[] buffData = new byte[BUFF_SIZE];

		@Override
		public void serialEvent(SerialPortEvent event) {
			synchronized(event){
				if(event.isRXCHAR() && event.getEventValue() > 0){
					try {
						// Время за которое выполняется передача буфера с добавление данных на график
						if (!isFreezeTime) {
							start = System.currentTimeMillis();
							isFreezeTime = true;
						}

//						start = System.nanoTime();
//						end = System.nanoTime();
//						System.out.println("dt="+(end-start));

						buffData = serialPort.readBytes(BUFF_SIZE);
//						lenBuffTmp = buffTmp.length;
//						System.out.println("lenBuffTmp = " + lenBuffTmp);

						// Интервал времени между получениями буферов N (текущего) и N+1 (следующего)
						timeRXBuffNp1 = System.currentTimeMillis();
						dTimeBetweenRXBuff = timeRXBuffNp1 - timeRXBuffN;
//						dTimeBetweenRXBuff /= 2;
						timeRXBuffN = timeRXBuffNp1;
						controllerCallback.rxByteBuff(buffData,  dTimeBetweenRXBuff / POINTS_IN_BUFF);
						log("dt between buff N and N+1 = " + dTimeBetweenRXBuff + " ms");

						// ... учитываем добавление данных на график
//						end = System.currentTimeMillis();
//						log("dt receive buffer and display in chart  = " + (end - start) + " ms");
//						isFreezeTime = false;

					} catch (SerialPortException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	private void log(String msg) {
		if (IS_LOG) System.out.println(UART.class.getSimpleName() + ": " + msg);
	}
}
