package controller;

import app.Const;
import model.UART;
import ui.UICalibrMonitor;
import ui.UICalibrMonitorCallback;

/**
 *
 */
public class ControllerCalibrMonitor implements ControllerCallback {


	private static final String MSG_STEP_1_SUCCESS = "Этап 1 выполнен успешно.";
	private static final String MSG_STEP_1_ERROR = "На этапе 1 возникла ошибка.";
	private static final String MSG_STEP_2_SUCCESS = "Этап 2 выполнен успешно.";
	private static final String MSG_STEP_2_ERROR = "На этапе 2 возникла ошибка.";


	/**
	 * Delay between send packages for valve quick open/close
	 *
	 * @see {@link UICalibrMonitor#jbtnOpenCloseValveQuick}
	 */
	private static final long delayMsOCVQ = 70;


	private UICalibrMonitorCallback uiCallback;

	private UART uart;





	public ControllerCalibrMonitor(UICalibrMonitorCallback uiCallback) {
		this.uiCallback = uiCallback;
	}

	public ControllerCalibrMonitor(UICalibrMonitorCallback uiCallback, UART uart) {
		this.uiCallback = uiCallback;
		this.uart = uart;
	}





	public void sendPackageToDevice(int[] pck) {
		uart.writeByteBuff(pck);
	}



	/*
	* ---------------------------------
	* 			REQUESTS
	* ---------------------------------
	 */

	/**
	 * Вход сервера в режим калибровки
	 */
	public void calibrModeMakeEnter() {
//		int[] cmdStop = {Const.CMD_STOP, 0, 0, 0};
		sendPackageToDevice(new int[]{Const.Calibr.CMD_MAKE_ENTER, 0, 0, 0});
	}

	/**
	 * Выход из режима калибровки
	 */
	public void calibrModeMakeExit() {
		sendPackageToDevice(new int[]{Const.Calibr.CMD_MAKE_EXIT, 0, 0, 0});
	}

	/**
	 * Открыть / закрыть эл. клапан
	 */
	public void calibrModeOpenCloseValve() {
		sendPackageToDevice(new int[]{Const.Calibr.CMD_OPEN_OR_CLOSE_VALVE, 0, 0, 0});
	}

	/**
	 * Быстрое пре открытие эл. клапана
	 */
	public void calibrModeOpenCloseValveQuick() {    // TODO: 15.01.19 - нужен ли этот метод, или вызывать с задержкой только предыдйщий ?
		sendPackageToDevice(new int[]{Const.Calibr.CMD_OPEN_OR_CLOSE_VALVE, 0, 0, 0});
		try {
			Thread.sleep(delayMsOCVQ);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		sendPackageToDevice(new int[]{Const.Calibr.CMD_OPEN_OR_CLOSE_VALVE, 0, 0, 0});
	}

	/**
	 * Ручное нагнетание
	 */
	public void calibrModeOpenClosePump() {
		sendPackageToDevice(new int[]{Const.Calibr.CMD_OPEN_OR_CLOSE_PUMP, 0, 0, 0});
	}

	/**
	 * Выполнить единичное преобразование
	 */
	public void calibrModeMakeSingleConv() {
		sendPackageToDevice(new int[]{Const.Calibr.CMD_MAKE_SINGLE_CONV, 0, 0, 0});
	}

	/**
	 * Откалибровать Offset.
	 * Этап 1
	 */
	public void calibrModeSetOffset() {
		sendPackageToDevice(new int[]{Const.Calibr.CMD_SET_OFFSET, 0, 0, 0});
	}

	/**
	 * Откалибровать FullScale
	 * Этап 2.
	 */
	public void calibrModeSetFullScale() {
		sendPackageToDevice(new int[]{Const.Calibr.CMD_SET_FULLSCALE, 0, 0, 0});
	}

	/**
	 * Проверить - сервер в режиме калибровки ?
	 */
	public void isInCalibrMode() {
		sendPackageToDevice(new int[]{Const.Calibr.CMD_IS_SERVER_IN_CALIBR_MODE, 0, 0, 0});
	}


	public void setSpeed(int steps) {
		sendPackageToDevice(new int[]{Const.Work.CMD_SET_SPEED, (byte) (steps & 0xFF), 0, 0});
	}



	@Override
	public void setPortState(UART.PortStates portStates) {
		// STUB for this frame ...
	}




	/*
	* ---------------------------------
	* 			RESPONSES
	* ---------------------------------
	 */

	@Override
	public void rxByteBuff(byte[] buff, long timeBetweenRXBuff) {
		decodePackageRSP(buff, timeBetweenRXBuff);
	}




	private static final int RX_PACKAGE_SIZE = 8;
	private int byteCount = 0;
	// 24 bit unsigned int data
	private int data0, data1;


	/**
	 * Make decoding responses package data.
	 *
	 * @param buff RSP package data
	 * @param extraMs - not used in this controller
	 */
	private void decodePackageRSP(byte[] buff, long extraMs) {
		byte rxPackage[] = new byte[RX_PACKAGE_SIZE];
		int entryVal;

		for (byte b : buff) {
			entryVal = (int) b & 0xFF; // convert to unsigned value
			switch (byteCount) {
				case 0:
					rxPackage[0] = (byte) entryVal;
					byteCount++;
					break;
				case 1:
					rxPackage[1] = (byte) entryVal;
					byteCount++;
					break;
				case 2:
					rxPackage[2] = (byte) entryVal;
					byteCount++;
					break;
				case 3:
					rxPackage[3] = (byte) entryVal;
					byteCount++;
					break;
				case 4:
					rxPackage[4] = (byte) entryVal;
					byteCount++;
					break;
				case 5:
					rxPackage[5] = (byte) entryVal;
					byteCount++;
					break;
				case 6:
					rxPackage[6] = (byte) entryVal;
					byteCount++;
					break;
				case 7:
					rxPackage[7] = (byte) entryVal;

					// get command
					int cmdCode = (rxPackage[0] & 0xFF) | ((rxPackage[1] & 0xFF) << 8);
					System.out.println("RSP cmdCode = " + cmdCode);

					// Processed only if RSP package related to calibration monitor.
					if (cmdCode >= Const.Calibr.L_RANGE && cmdCode <= Const.Calibr.R_RANGE) {

						if (cmdCode == Const.Calibr.CMD_SET_OFFSET) {
							data0 = (rxPackage[2] & 0xFF) | ((rxPackage[3] & 0xFF) << 8) | ((rxPackage[4] & 0xFF) << 16);
							uiCallback.logStatus(MSG_STEP_1_SUCCESS + " Offset reg = " + data0);
							data0 = 0;
						}
						else if (cmdCode == Const.Calibr.CMD_SET_FULLSCALE) {
							data0 = (rxPackage[2] & 0xFF) | ((rxPackage[3] & 0xFF) << 8) | ((rxPackage[4] & 0xFF) << 16);
							uiCallback.logStatus(MSG_STEP_2_SUCCESS + " Full-scale reg = " + data0);
							data0 = 0;
						}
						else if (cmdCode == Const.Calibr.CMD_MAKE_SINGLE_CONV) {
							data0 = (rxPackage[2] & 0xFF) | ((rxPackage[3] & 0xFF) << 8) | ((rxPackage[4] & 0xFF) << 16);
							uiCallback.setCurrPrs(data0);
							data0 = 0;
						}

					}


					byteCount = 0;
					break;
			}
		}
	}
}
