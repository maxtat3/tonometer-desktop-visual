package controller;

import app.Const;
import model.UART;
import ui.UISensorSelect;

/**
 * Controller for {@link UISensorSelect}
 */
public class ControllerSensorSelect {

	private UART uart;


	public ControllerSensorSelect(UART uart) {
		this.uart = uart;
	}


	/*
	* ---------------------------------
	* 			REQUESTS
	* ---------------------------------
	 */

	/**
	 * Работа с датчиком {@link UISensorSelect#TXT_MPX_2053}
	 */
	public void selectMpx2053() {
		sendPackageToDevice(new int[]{Const.Calibr.CMD_SELECT_SENSOR, Const.Calibr.DATA_SELECT_SENSOR_MPX2053, 0, 0});
	}

	/**
	 * Работа с датчиком {@link UISensorSelect#TXT_24_PCB_FA_6G}
	 */
	public void select24pcbfa6g() {
		sendPackageToDevice(new int[]{Const.Calibr.CMD_SELECT_SENSOR, Const.Calibr.DATA_SELECT_SENSOR_24PCBFA6G, 0, 0});
	}


	public void sendPackageToDevice(int[] pck) {
		uart.writeByteBuff(pck);
	}
}
