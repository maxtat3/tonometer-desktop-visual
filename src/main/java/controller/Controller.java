package controller;

import app.Const;
import com.nicu.tncnlib.calc.BPHRDetector;
import com.nicu.tncnlib.calc.Filter;
import com.nicu.tncnlib.system.BattCharge;
import model.UART;
import ui.UICallback;
import util.AppProperties;
import util.Recorder;

import javax.swing.*;
import java.util.ArrayList;

/**
 *
 */
public class Controller implements ControllerCallback {

	private static final boolean IS_LOG = true;

	/**
	 * Data model for communicate with device.
	 * Created in this class.
	 */
	private UART uart;

	/**
	 * Callback for UI.
	 * Using it callback, Controller must be send data to UI.
	 *
	 * @see ui.UI
	 * @see Controller
	 */
	private UICallback uiCallback;

	private Filter filters;

	private BPHRDetector bphrDetector;

	// Assuming that the measurement process can take about 1.5 minutes
	private static final int CAPACITY_RES_LIST = 50000; // points
	private java.util.ArrayList<Double> prsResults = new ArrayList<>(CAPACITY_RES_LIST);
	private java.util.ArrayList<Double> pulseResults = new ArrayList<>(CAPACITY_RES_LIST);

	private Thread battChargeUpdater, msrStopwatchUpdater;

	/**
	 * Is started measure process action.
	 * true - process started.
	 */
	private boolean isStartAction = false;

	private static int pointsPass;
	private int pointsPassedCount = 0;


	public Controller(UICallback uiCallback) {
		this.uiCallback = uiCallback;
		initFilters();
	}

	public Controller(UICallback uiCallback, UART uart) {
		this.uiCallback = uiCallback;
		this.uart = uart;
		initFilters();
		bphrDetector = new BPHRDetector();
		msrStopwatchUpdater = new MeasureStopwatchUpdater();
	}

	/**
	 * Initialisation filters from properties file.
	 */
	private void initFilters() {
		filters = new Filter();
		boolean enDCrm = Boolean.parseBoolean(AppProperties.get(AppProperties.Options.FILTER_DCRM_EN));
		double alphaDCrm = Double.parseDouble(AppProperties.get(AppProperties.Options.FILTER_DCRM_ALPHA));
		boolean enBW = Boolean.parseBoolean(AppProperties.get(AppProperties.Options.FILTER_BW_EN));
		String prestBWStr = AppProperties.get(AppProperties.Options.FILTER_BW_ADCPREST);
		Filter.PrestBW prestBW = Filter.PrestBW.ADC_73_10;
		int i = 0;
		for (Filter.PrestBW p : Filter.PrestBW.values()) {
			if (p.toString().equals(prestBWStr)) {
				prestBW = p;
				break;    // bw filter prest is found in property file - exit loop
			}
		}
		filters.init(enDCrm, alphaDCrm, enBW, prestBW);

		pointsPass = prestBW.getFs() / 2;
	}



    /*
     * -------------------------------------------
     *      UI MUST BE CALLED NEXT PUBLIC METHODS
     * -------------------------------------------
     */

	/**
	 * Turning on and do initialization UART module in the device.
	 * The parameter must be to be transmitted port name from this
	 * {@link UART#NUMBERS_OF_COM_PORTS} list.
	 * In begin checking is open default COM port. When port opened,
	 * he is closed, and applied new port name - open new port.
	 *
	 * @param portName UART COM port name
	 * @see UART.DefaultCOMPort
	 */
	public void connectToDevice(String portName) {
		try {
			if (isOpenUSARTPort()) {
				closeUSARTPort();
				uart.init(portName);
			} else {
				uart.init(portName);
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Send text string to opened COM port UART module in the device .
	 *
	 * @param pck transmitted to open COM port text as String
	 */
	public void sendPackageToDevice(int[] pck) {
		uart.writeByteBuff(pck);
	}

	/**
	 * Check state - is open UART COM Port ?
	 *
	 * @return true - is COM port open
	 */
	public boolean isOpenUSARTPort() {
		return uart.isOpen();
	}

	/**
	 * Close COM port.
	 *
	 * @return true - is COM port closed
	 */
	public boolean closeUSARTPort() {
		return uart.close();
	}

	/**
	 * Start or stop measure process
	 */
	public void doStartStopMsr() {
		//стоп измерений
		if (isStartAction) {
			int[] cmdStop = {Const.Work.CMD_STOP, 0, 0, 0};
			sendPackageToDevice(cmdStop);
			isStartAction = false;
			Recorder.writeResultsToFile(prsResults, pulseResults);
			pointsPassedCount = 0;
			double[] diaAndSys = bphrDetector.calculateDiaAndSysPressure(prsResults, pulseResults);
			uiCallback.showDiaSys(diaAndSys[0], diaAndSys[1]);
			long elapsed = System.currentTimeMillis() - startMsrTime;
			System.out.println("elapsed = " + elapsed);
			try {
				msrStopwatchUpdater.join();
				// Wait for updater to finish
			} catch (InterruptedException ie) {
				System.err.println(ie);
			}
			showElapsedMsrTime(elapsed);
			prsResults.clear();
			pulseResults.clear();
			System.out.println("stop msr");

		}else{
			//старт измерений
			int[] cmdStart = {Const.Work.CMD_START, 0, 0, 0};
			sendPackageToDevice(cmdStart);
			uart.startedTimeMs(System.currentTimeMillis());
			isStartAction = true;
			isRunningMsrStopwatch = false;	// wait to pass first points
			uiCallback.resetChartData();
			prsResults.clear();    // TODO: 08.04.19 May be redundant clear lists with msr start ?
			pulseResults.clear();
			System.out.println("start msr");
		}

	}



    /*
     * -------------------------------
     *   RETURNED VALUES FROM MODEL
     * -------------------------------
     */

	@Override
	public void setPortState(UART.PortStates portStates) {
		uiCallback.setPortState(portStates);
	}

	@Override
	public void rxByteBuff(byte[] buff, long timeBetweenRXBuff) {
		decodePackage(buff, timeBetweenRXBuff);
	}


	// TODO: 30.03.19 See in mobile: view # MainActivity
	// Just about max value in 300 mmHg
	public static final int ADC_MAX_VAL = 7300000;

	// Maximum value of measured pressure
	public static final int PRS_MM_HG_MAX_VAL = 300;

	public static final double prsDivider = PRS_MM_HG_MAX_VAL / (double) ADC_MAX_VAL;

	private static final int RX_PACKAGE_SIZE = 8;
	private int byteCount = 0;
	private int rawPrs, rawPulseCapa;
	private double pulseFiltered, currPrs;

	private void decodePackage(byte[] buff, long extraMs) {
		byte rxPackage[] = new byte[RX_PACKAGE_SIZE];
		int entryVal;

		for (byte b : buff) {
			entryVal = (int) b & 0xFF; // convert to unsigned value
			switch (byteCount) {
				case 0:
					rxPackage[0] = (byte) entryVal;
					byteCount++;
					break;
				case 1:
					rxPackage[1] = (byte) entryVal;
					byteCount++;
					break;
				case 2:
					rxPackage[2] = (byte) entryVal;
					byteCount++;
					break;
				case 3:
					rxPackage[3] = (byte) entryVal;
					byteCount++;
					break;
				case 4:
					rxPackage[4] = (byte) entryVal;
					byteCount++;
					break;
				case 5:
					rxPackage[5] = (byte) entryVal;
					byteCount++;
					break;
				case 6:
					rxPackage[6] = (byte) entryVal;
					byteCount++;
					break;
				case 7:
					rxPackage[7] = (byte) entryVal;

					// get command
					int cmdCode = (rxPackage[0] & 0xFF) | ((rxPackage[1] & 0xFF) << 8);

					// make measuring data - get pressure and rawPulseCapa
					if (cmdCode == Const.Work.CMD_START) {
						rawPrs = (rxPackage[2] & 0xFF) | ((rxPackage[3] & 0xFF) << 8) | ((rxPackage[4] & 0xFF) << 16);
						rawPulseCapa = (rxPackage[5] & 0xFF) | ((rxPackage[6] & 0xFF) << 8) | ((rxPackage[7] & 0xFF) << 16);
//						log("pulse=" + rawPulseCapa + " | " + "prs=" + rawPrs);

						pulseFiltered = filters.filtration(rawPrs);
						if (++pointsPassedCount > pointsPass) {
							if (!isRunningMsrStopwatch) {	// protecting cyclic call
								isRunningMsrStopwatch = true;
								startMsrTime = System.currentTimeMillis();
								msrStopwatchUpdater = new MeasureStopwatchUpdater();
								msrStopwatchUpdater.start();
							}

							currPrs = rawPrs * prsDivider;

							prsResults.add(currPrs);
							pulseResults.add(pulseFiltered);	// FIXME ??? pulseFiltered * 1000  see csv

							uiCallback.addPoints((int) pulseFiltered, extraMs);
							uiCallback.showPressure((int) Math.round(currPrs));
						}
					}
					else if (cmdCode == Const.Work.CMD_CODE_GET_INFO_BATT_CHARGE) {
						int charge = rxPackage[2];
						charge = charge & 0xFF;		// convert to unsigned
						uiCallback.showBattCharge(BattCharge.calcBattCharge(charge));
						System.out.println(charge + " | " + BattCharge.calcBattCharge(charge));
					}

					byteCount = 0;
					break;
			}
		}
	}



	private static final String TIME_FORMAT_PATTERN = "mm:ss";
	private long startMsrTime;
	private final static java.text.SimpleDateFormat TIMER_FORMAT = new java.text.SimpleDateFormat(TIME_FORMAT_PATTERN);
	// Use as protect to cyclic call in server response block
	// See {@link Const.Work.CMD_START} processing block
	private boolean isRunningMsrStopwatch = false;

	private final Runnable msrTimeUpdater = new Runnable() {
		public void run() {
			showElapsedMsrTime(System.currentTimeMillis() - startMsrTime);
		}
	};

	private class MeasureStopwatchUpdater extends Thread {
		@Override
		public void run() {
			try {
				while (isStartAction) {
					SwingUtilities.invokeAndWait(msrTimeUpdater);
					Thread.sleep(500);
				}
			} catch (java.lang.reflect.InvocationTargetException ite) {
				ite.printStackTrace(System.err);
			} catch (InterruptedException ie) {
				ie.printStackTrace();
			}
		}
	}

	private void showElapsedMsrTime(long elapsedTime) {
		uiCallback.showElapsedMsrTime(TIMER_FORMAT.format(new java.util.Date(elapsedTime)));
	}




	public static final int BATT_CHARGE_LISTEN_TIME_INTERVAL = 10;	// in seconds

	public void startListenBattCharge() {
		isStartAction = false;
		battChargeUpdater = new BattChargeUpdater();
		battChargeUpdater.start();
	}

	private class BattChargeUpdater extends Thread {
		@Override
		public void run() {
			while (true) {
				if (!isStartAction) getBattCharge();
				try {
					sleep(BATT_CHARGE_LISTEN_TIME_INTERVAL * 1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void getBattCharge() {
		uart.writeByteBuff(new int[]{Const.Work.CMD_CODE_GET_INFO_BATT_CHARGE, 0, 0, 0});
	}

	// TODO: 29.03.19 See APIToMCU # setSpeed method
	public boolean setSpeed(int value) {
		if (value > 100) return false;

		int[] request = new int[4];
		request[0] = Const.Work.CMD_SET_SPEED & 0xFF;
		request[1] = (byte) (value & 0xFF);
		request[2] = 0;	// not contain data
		request[3] = 0; // ---//---
		uart.writeByteBuff(request);
		return true;
	}


	private void log(String msg) {
		if (IS_LOG) System.out.println(Controller.class.getSimpleName() + ": " + msg);
	}

}
