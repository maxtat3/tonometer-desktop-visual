package controller;

import model.UART;
import ui.UICallback;


/**
 *
 */
public interface ControllerCallback {

	/**
	 * Model inform Controller of the COM port state.
	 * Controller must be set port state in UI.
	 *
	 * @param portStates available UART port states lists
	 *        in {@link UART.PortStates}
	 */
	void setPortState(UART.PortStates portStates);

	/**
	 * Model translate received data from
	 * UART COM port to Controller. In the Controller do processed
	 * this received data and notify UI actions.
	 * For example UI actions: changed port state, add point data to chart, etc.
	 * Full notify actions see {@link UICallback} interface.
	 * Model does not processed any received data from UART COM port.
	 *
	 * @param buff data and/or commands from UART COM port.
	 * @param timeBetweenRXBuff
	 */
	void rxByteBuff(byte[] buff, long timeBetweenRXBuff);    // TODO: 03.09.18 rn dtRXTwoBuffers

}
