package util;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 * Record result data in file and save in FS.
 */
public class Recorder {

	public static final boolean IS_LOG = false;
	public static final String CSV_EXT_FILE = ".csv";
	public static final String PREFIX_FILE = "results_";

	public static final String LFCR = "\n";
	public static final String TAB = "\t";


	/**
	 * Written results from list to file.
	 * After clear all data in this lists.
	 *
	 * @param list list parsed to file
	 */
	public static void writeResultsToFile(List<Double> list) {	// TODO: 01.04.19 Number extends <- Integer, Double
//		if (list.isEmpty()) return;		// TODO: 01.04.19 is empty check

		StringBuilder sb = new StringBuilder(list.size());

		// TODO: 17.09.18 think about remain this exception or not ?
//		if (listA.size() != listB.size()) throw new NumberFormatException(
//			"Both lists must be equals size, but list A size = " + listA.size() + " list B size = " + listB.size()
//		);

		for (int i = 0; i < list.size(); i++) {
			sb.append(i);
			sb.append(TAB);
			sb.append(list.get(i));
			sb.append(LFCR);
		}
		list.clear();
		write(sb);
	}

	/**
	 * Written results from two lists to file.
	 * Check size of all lists - results are saved form at the lowest list size.
	 *
	 * @param listA first list
	 * @param listB second list
	 */
	public static void writeResultsToFile(List<Double> listA, List<Double> listB) {
		// find minimum size of lists
		int sizeA = listA.size();
		int sizeB = listB.size();
		int sizeMin = sizeA;
		if (sizeB != 0 && sizeMin > sizeB) {
			sizeMin = sizeB;
		}

		StringBuilder sb = new StringBuilder(sizeMin);

		// TODO: 17.09.18 think about remain this exception or not ?
//		if (listA.size() != listB.size()) throw new NumberFormatException(
//			"Both lists must be equals size, but list A size = " + listA.size() + " list B size = " + listB.size()
//		);

		for (int i = 0; i < listA.size(); i++) {
			sb.append(i);
			sb.append(TAB);
			sb.append(listA.get(i));
			sb.append(TAB);
			sb.append(listB.get(i));
			sb.append(LFCR);
		}

		write(sb);
	}

	private static void write(StringBuilder sb) {
		FileWriter file;
		try {
			file = new FileWriter("./" + PREFIX_FILE + getCurrentDate() + CSV_EXT_FILE);    // TODO: 17.09.18 make test in Win OS
			file.write(String.valueOf(sb));
			file.flush(); //clear buffer and write to file
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private static String getCurrentDate() {
		return new SimpleDateFormat("dd_MM_yyyy__HH_mm_ss", java.util.Locale.getDefault()).format(new Date());
	}

	/**
	 * Output logging to stdio for this class.
	 *
	 * @param msg logging massage
	 */
	private static void log(String msg) {
		if (IS_LOG) System.out.println(Recorder.class.getSimpleName() + ": " + msg);
	}

}
