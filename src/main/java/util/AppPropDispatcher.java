package util;


import com.nicu.tncnlib.calc.Filter;

import javax.swing.*;
import java.io.IOException;


public class AppPropDispatcher {

	private static final boolean IS_LOG = false;

	/**
	 * Check valid configuration in application properties file.
	 *
	 * @See {@link AppProperties}
	 */
	public static void check() {
		boolean errFlag = false;
		StringBuilder err = new StringBuilder(1000);
		err.append("<html>");

		// Parse DC-remove active state
		boolean enDCrm = true;
		String enDCrmStr = AppProperties.get(AppProperties.Options.FILTER_DCRM_EN);
		if (enDCrmStr != null && (enDCrmStr.equals(AppProperties.Options.TRUE) || enDCrmStr.equals(AppProperties.Options.FALSE))) {
			enDCrm = Boolean.parseBoolean(enDCrmStr);
		} else {
			err.append("Checking the DC-rm filter activity from the configuration failed. <br>");
			err.append("By default DC-rm filter is enabled. <br><br>");
			AppProperties.set(AppProperties.Options.FILTER_DCRM_EN, AppProperties.Options.TRUE);
			errFlag = true;
		}

		// Parse DC-remove alpha value
		double alphaDCrm = 0.95;	// default value
		String alphaDCrmStr = AppProperties.get(AppProperties.Options.FILTER_DCRM_ALPHA);
		try {
			alphaDCrm = Double.parseDouble(alphaDCrmStr);
		} catch (NullPointerException | NumberFormatException e) {
			err.append("Checking the DC-rm alpha from the configuration failed. <br>");
			err.append("By default DC-rm alpha set 0.95 <br><br>");
			AppProperties.set(AppProperties.Options.FILTER_DCRM_ALPHA, String.valueOf(alphaDCrm));
			errFlag = true;
		}

		// Parse Butterworth filter active state
		boolean enBW = true;
		String enBWStr = AppProperties.get(AppProperties.Options.FILTER_BW_EN);
		if (enBWStr != null && (enBWStr.equals(AppProperties.Options.TRUE) || enBWStr.equals(AppProperties.Options.FALSE))) {
			enBW = Boolean.parseBoolean(enBWStr);
		} else {
			err.append("Checking the BW filter activity from the configuration failed. <br>");
			err.append("By default BW filter is enabled. <br><br>");
			AppProperties.set(AppProperties.Options.FILTER_BW_EN, AppProperties.Options.TRUE);
			errFlag = true;
		}

		// Parse prest Butterworth filter from property file
		String prestBWStr = AppProperties.get(AppProperties.Options.FILTER_BW_ADCPREST);
		Filter.PrestBW prestBW = Filter.PrestBW.ADC_73_10;
		int i = 0;
		for (Filter.PrestBW p : Filter.PrestBW.values()) {
			if (p.toString().equals(prestBWStr)) {
				log("found bw filter prest !");
				prestBW = p;
				break;	// bw filter prest is found in property file - exit loop
			}

			// BW filter prest not found in property file - save default value
			if (i == Filter.PrestBW.values().length - 1){
				log("bw filter prest not found");
				String fsfcDef = String.valueOf(prestBW.getFs()) + "-" + String.valueOf(prestBW.getFc());
//				log(fsfcDef);
				err.append("Incorrect BW prest value of ADC settings in configuration file. <br>");
				err.append("By default set Fs=" + prestBW.getFs() + " Hz," + " Fc=" + prestBW.getFc() + " Hz" + "<br><br>");
				AppProperties.set(AppProperties.Options.FILTER_BW_ADCPREST, fsfcDef);
				errFlag = true;
			}

			i++;
		}
		log("set BW prest: " + prestBW.getFs() + "-" + prestBW.getFc());

		// Other actions
		err.append("</html>");
		if (errFlag) {
			try {
				AppProperties.save();
			} catch (IOException e) {
				e.printStackTrace();
			}
			JOptionPane.showMessageDialog(null, err.toString(), "Warning", JOptionPane.WARNING_MESSAGE);
		}
	}

	private static void log(String msg) {
		if(IS_LOG) System.out.println(AppPropDispatcher.class.getSimpleName() + ": " + msg);
	}
}
