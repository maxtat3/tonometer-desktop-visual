package util;

import java.io.*;
import java.util.Properties;


/**
 * This class provides the configuration services to the rest of the app.
 */
public class AppProperties {

	private static final boolean isLog = true;


	public class Options {
		public static final String CONN_TO_SERVER_AUTO = "conn.auto";
		public static final String CONN_PORT_NAME = "conn.port";
		public static final String FILTER_DCRM_EN = "filter.dcrm.en";
		public static final String FILTER_DCRM_ALPHA = "filter.dcrm.alpha";
		public static final String FILTER_BW_EN = "filter.bw.en";
		public static final String FILTER_BW_ADCPREST = "filter.bw.prest";

		/** String representation <tt>true</tt> default value **/
		public static final String TRUE = "true";
		/** String representation <tt>false</tt> default value **/
		public static final String FALSE = "false";
	}


	private static final String SYS_PROP_USR_HOME = System.getProperty("user.home");
	private static final String SYS_PROP_FILE_SEP = System.getProperty("file.separator");

	private static final String PROP_FILE_NAME = "tonometer-mk-ua-desktop.properties";
	private static final String PROP_FILE = SYS_PROP_USR_HOME + SYS_PROP_FILE_SEP + PROP_FILE_NAME;
	private static Properties props;
	private static String propFilePath;


	public static void load() throws IOException {
		if(!System.getProperties().containsKey(PROP_FILE_NAME)) {
			log("Not find System.getProperties");

			if(OSUtils.isLinux()) {
				String configBase = System.getenv("XDG_CONFIG_HOME");
				if(null == configBase || configBase.trim().equals("")) {
					log("Not find XDG_CONFIG_HOME");
					configBase = SYS_PROP_USR_HOME + SYS_PROP_FILE_SEP + ".config";
				}
				System.setProperty(PROP_FILE_NAME, configBase + SYS_PROP_FILE_SEP + PROP_FILE_NAME);
			}
			else if(OSUtils.isMacOSX()) {
				System.setProperty(PROP_FILE_NAME, SYS_PROP_USR_HOME + SYS_PROP_FILE_SEP + "Library" + SYS_PROP_FILE_SEP + "Preferences" + SYS_PROP_FILE_SEP + PROP_FILE_NAME);
			}
		}

		propFilePath = System.getProperty(PROP_FILE_NAME);
		if (propFilePath == null || propFilePath.trim().equals("")) {
			log("Not find propertiesFile, set it to: " + PROP_FILE);
			propFilePath = PROP_FILE;
		}

		File prefs = new File(propFilePath);
		prefs.getParentFile().mkdirs();

		//Attempt to load the properties
		try {
			log("Loading the properties file [" + propFilePath + "]");
			props = new Properties();
			props.load(new FileInputStream(propFilePath));
		} catch (FileNotFoundException e) {
			log("Property file not found. Will be created the next time the properties are saved.");
		}
	}


	public static String get(String name, String defaultValue) {
		String retVal = props.getProperty(name, defaultValue);
		log("Returning the property, name=" + name + ", value=" + retVal);
		return retVal;
	}

	public static String get(String name) {
		return get(name, null);
	}

	public static int getInt(String name, int defaultValue) {
		String cfgVal = props.getProperty(name);
		int ret = defaultValue;
		if (cfgVal != null && Utils.isNumeric(cfgVal)) {
			ret = Integer.parseInt(cfgVal);
		}
		log("Returning the property, name=" + name + ", value=" + ret);
		return ret;
	}

	public static void set(String name, String value) {
		log("Setting the property, name=" + name + ", value=" + value);
		props.setProperty(name, value);
	}

	public static void save() throws IOException {
		log("Saving properties to the file [" + propFilePath + "]");
		props.store(new FileOutputStream(propFilePath), "Tonometer");
	}


	private static void log(String msg) {
		if(isLog) System.out.println(AppProperties.class.getSimpleName() + ": " + msg);
	}

}
