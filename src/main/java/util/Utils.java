package util;

import java.text.NumberFormat;
import java.text.ParsePosition;


/**
 * General utils for app.
 */
public class Utils {

	public static final int BIT_CAPACITY_OF_DIGITAL_POT = 6;

	/**
	 * Converted percentage value to steps for digital potentiometer.
	 *
	 * @param percentage percents in range [1...100]
	 * @return amount of steps for changed resistance value in digital potentiometer,
	 * or error code 101 if parameter percentage more 100.
	 */
	public static int convertPercentageToSteps(int percentage) {
		if (percentage > 100) return 101;	// error code
		return (int) ((Math.pow(2, BIT_CAPACITY_OF_DIGITAL_POT) / 100) * percentage);
	}

	/**
	 * Checks whether the String a valid numeric.
	 *
	 * @param str <tt>String</tt> to check
	 * @return <tt>true</tt> if the string is a correctly formatted number
	 */
	public static boolean isNumeric(String str){
		NumberFormat formatter = NumberFormat.getInstance();
		ParsePosition pos = new ParsePosition(0);
		formatter.parse(str, pos);
		return str.length() == pos.getIndex();
	}

}
