package app;

/**
 * This class describes of interface commands.
 * Full list of commands see in:
 * https://docs.google.com/document/d/1RQgnP7-pW4waZCCzdTPicrtOSrmcalR7C-7UWGqbjSA/
 * https://docs.google.com/spreadsheets/d/17ZdH-s2sm5Swhw24LjUi1gB6xRLtGfkRxGjjJAGPl9g/
 */
public class Const {

	/**
	 * Working commands of device.
	 * Рабочий режим прибора (основная программа).
	 *
	 * All commands in dec presentation.
	 */
	public static class Work {

		/* Range of command values for these commands */
		/** Left range of commands. This number is in range. **/
		public static final int L_RANGE = 1;

		/** Right range of commands. This number is NOT in range. **/
		public static final int R_RANGE = 23;


		/* Command codes */
		/** Запуск измерений. **/
		public static final int CMD_START = 1;

		/** Остановка измерений. **/
		public static final int CMD_STOP = 2;

		/* Определение остатка заряда АКБ */
		public static final int CMD_CODE_GET_INFO_BATT_CHARGE = 0x03;

		/** Установка скорости нагнетания. **/
		public static final int CMD_SET_SPEED = 4;
	}


	/**
	 * Calibration mode.
	 * Режим калибровки.
	 *
	 * All commands in dec presentation.
	 */
	public static class Calibr {

		/* Range of command values for these commands */
		/** Left range of commands. This number is in range. **/
		public static final int L_RANGE = 24;

		/** Right range of commands. This number is NOT in range. **/
		public static final int R_RANGE = 35;


		/* Command codes */
		/** Вход в режим калибровки **/
		public static final int CMD_MAKE_ENTER = 24;

		/** Выход из режима калибровки **/
		public static final int CMD_MAKE_EXIT = 25;

		/** Проверить - сервер в режиме калибровки ? **/
		public static final int CMD_IS_SERVER_IN_CALIBR_MODE = 26;

		/*---*/

		/** Открыть / закрыть эл. клапан  **/
		public static final int CMD_OPEN_OR_CLOSE_VALVE = 27;

		/** Быстрое пре открытие эл. клапана **/
		private static final int CMD_OPEN_OR_CLOSE_VALVE_QUICK = 27;

		/** Ручное нагнетание **/
		public static final int CMD_OPEN_OR_CLOSE_PUMP = 28;

		/** Выполнить единичное преобразование **/
		public static final int CMD_MAKE_SINGLE_CONV = 29;

		/*---*/

		/** Откалибровать Offset, шаг 1 **/
		public static final int CMD_SET_OFFSET = 30;

		/** Откалибровать FullScale, шаг 2 **/
		public static final int CMD_SET_FULLSCALE = 31;

		/*---*/

		/** Выбор датчика **/
		public static final int CMD_SELECT_SENSOR = 32;
		public static final int DATA_SELECT_SENSOR_MPX2053 = 1;
		public static final int DATA_SELECT_SENSOR_24PCBFA6G = 2;
	}
}

