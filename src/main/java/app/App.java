package app;

import org.jfree.ui.RefineryUtilities;
import ui.UI;
import util.AppPropDispatcher;
import util.AppProperties;

import java.io.IOException;

public class App {

	public static void main(String[] args) throws IOException {
		AppProperties.load();
		AppPropDispatcher.check();

		new UI("Tonometer desktop oscillogram visual");
	}
}
