package ui;

import controller.ControllerCalibrMonitor;
import model.UART;
import util.Utils;

import javax.swing.*;
import javax.swing.text.DefaultCaret;
import java.awt.*;
import java.awt.event.*;


/**
 * Calibration monitor UI frame.
 */
public class UICalibrMonitor extends JDialog implements UICalibrMonitorCallback {

	private static final boolean isLog = true;

	public static final int KEY_OPEN_CLOSE_VALVE = KeyEvent.VK_Q;	// open or close valve
	public static final int KEY_OPEN_CLOSE_PUMP = KeyEvent.VK_W;	// enable or disable pump
	public static final int KEY_OPEN_CLOSE_VALVE_QUICK = KeyEvent.VK_E;	// quick open or close valve (with delay)

	private javax.swing.JSlider jSliderPumpSpeed;
	private javax.swing.JButton jbtnEnPump;
	private javax.swing.JButton jbtnOpenCloseValve;
	private javax.swing.JButton jbtnOpenCloseValveQuick;
	private javax.swing.JButton jbtnSetFullScaleCalibr;
	private javax.swing.JButton jbtnSetZeroScaleCalibr;
	private javax.swing.JLabel jlAirPumpSetSpeed;
	private javax.swing.JLabel jlCurrPrs;
	private javax.swing.JLabel jlCurrPrs1;
	private javax.swing.JLabel jlCurrPrsVal;
	private javax.swing.JLabel jlFirstStepInfo0;
	private javax.swing.JLabel jlFirstStepInfo1;
	private javax.swing.JLabel jlFirstStepInfo2;
	private javax.swing.JLabel jlSecondStepInfo0;
	private javax.swing.JLabel jlSecondStepInfo1;
	private javax.swing.JLabel jlSecondStepInfo2;
	private javax.swing.JPanel jp1;
	private javax.swing.JPanel jp2;
	private javax.swing.JPanel jpLog;
	private javax.swing.JScrollPane jscpLog;
	private javax.swing.JTextArea jtaLogVal;
	private javax.swing.text.DefaultCaret caretLog;


	private JFrame parentFrame;
	private ControllerCalibrMonitor controller;


	public UICalibrMonitor(JFrame parentFrame, UART uart) {
		super(parentFrame);
		this.parentFrame = parentFrame;

		if (parentFrame != null) {
			Dimension parentSize = parentFrame.getSize();
			Point p = parentFrame.getLocation();
			setLocation(p.x + parentSize.width / 4, p.y + parentSize.height / 4);
		}

		controller = new ControllerCalibrMonitor(this, uart);
		uart.addControllerCallback(controller);

		buildUI();
		setListeners();
		setVisible(true);
	}

	private void buildUI() {
		jp1 = new javax.swing.JPanel();
		jlFirstStepInfo0 = new javax.swing.JLabel();
		jlFirstStepInfo1 = new javax.swing.JLabel();
		jlFirstStepInfo2 = new javax.swing.JLabel();
		jbtnSetZeroScaleCalibr = new javax.swing.JButton();
		jp2 = new javax.swing.JPanel();
		jlSecondStepInfo0 = new javax.swing.JLabel();
		jbtnOpenCloseValve = new javax.swing.JButton();
		jlSecondStepInfo1 = new javax.swing.JLabel();
		jbtnEnPump = new javax.swing.JButton();
		jbtnOpenCloseValveQuick = new javax.swing.JButton();
		jlAirPumpSetSpeed = new javax.swing.JLabel();
		jSliderPumpSpeed = new javax.swing.JSlider();
		jlCurrPrs = new javax.swing.JLabel();
		jlCurrPrsVal = new javax.swing.JLabel();
		jlCurrPrs1 = new javax.swing.JLabel();
		jlSecondStepInfo2 = new javax.swing.JLabel();
		jbtnSetFullScaleCalibr = new javax.swing.JButton();
		jpLog = new javax.swing.JPanel();
		jscpLog = new javax.swing.JScrollPane();
		jtaLogVal = new javax.swing.JTextArea();
		caretLog = (DefaultCaret)jtaLogVal.getCaret();

		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Монитор калибровки");
		setResizable(false);

		jp1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "1. Калибровка сдвига нуля", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N
		jp1.setToolTipText("");

		jlFirstStepInfo0.setText("Отключите манжету");

		jlFirstStepInfo1.setText("Не должно быть внешнего воздействия на устройство");

		jlFirstStepInfo2.setText("Нажмите следующую кнопку");

		jbtnSetZeroScaleCalibr.setText("Калибровка сдвига нуля (этап 1)");

		javax.swing.GroupLayout jp1Layout = new javax.swing.GroupLayout(jp1);
		jp1.setLayout(jp1Layout);
		jp1Layout.setHorizontalGroup(
			jp1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jp1Layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(jp1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
						.addComponent(jbtnSetZeroScaleCalibr, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(jlFirstStepInfo0, javax.swing.GroupLayout.Alignment.LEADING)
						.addComponent(jlFirstStepInfo1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(jlFirstStepInfo2, javax.swing.GroupLayout.Alignment.LEADING))
					.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		jp1Layout.setVerticalGroup(
			jp1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jp1Layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(jlFirstStepInfo0, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
					.addComponent(jlFirstStepInfo1)
					.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
					.addComponent(jlFirstStepInfo2)
					.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
					.addComponent(jbtnSetZeroScaleCalibr)
					.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);

		jp2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "2. Калибровка ошибки усиления", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N

		jlSecondStepInfo0.setText("Подсоедините манжету через тройник");

		jbtnOpenCloseValve.setText("<html><center>Клапан откр / закр </center></html>");

		jlSecondStepInfo1.setText("Выполните нагнетание воздуха строго до 200 мм.рт.ст.");

		jbtnEnPump.setText("<html> <center> Компрессор<br>вкл / выкл </center> </html>");
		jbtnEnPump.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

		jbtnOpenCloseValveQuick.setText("<html><center>Приоткрыть<br>клапан</center></html>");

		jlAirPumpSetSpeed.setText("Скорость (%):");

		jSliderPumpSpeed.setMajorTickSpacing(20);
		jSliderPumpSpeed.setMinorTickSpacing(5);
		jSliderPumpSpeed.setPaintLabels(true);
		jSliderPumpSpeed.setPaintTicks(true);
		jSliderPumpSpeed.setToolTipText("Установка скорости нагнетания в пневмо-систему");

		jlCurrPrs.setText("Давление в системе:");
		jlCurrPrs.setToolTipText("Текущее давление в пневмо-системе");

		jlCurrPrsVal.setFont(new java.awt.Font("Dialog", Font.BOLD, 24)); // NOI18N
		jlCurrPrsVal.setText("---");

		jlCurrPrs1.setFont(new java.awt.Font("Dialog", Font.BOLD, 18)); // NOI18N
		jlCurrPrs1.setText("мм.рт.ст.");

		jlSecondStepInfo2.setText("<html>После достижения 200 мм.рт.ст. по показаниям<br>внешнего манометра нажмите следующую кнопку</html>");

		jbtnSetFullScaleCalibr.setText("Калибровка ошибки усиления (этап 2)");

		javax.swing.GroupLayout jp2Layout = new javax.swing.GroupLayout(jp2);
		jp2.setLayout(jp2Layout);
		jp2Layout.setHorizontalGroup(
			jp2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jp2Layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(jp2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addComponent(jbtnSetFullScaleCalibr, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(jbtnOpenCloseValve)
						.addGroup(jp2Layout.createSequentialGroup()
							.addComponent(jbtnEnPump, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
							.addComponent(jbtnOpenCloseValveQuick, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
							.addGroup(jp2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(jp2Layout.createSequentialGroup()
									.addGap(31, 31, 31)
									.addComponent(jlAirPumpSetSpeed)
									.addGap(0, 0, Short.MAX_VALUE))
								.addGroup(jp2Layout.createSequentialGroup()
									.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addComponent(jSliderPumpSpeed, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))))
						.addGroup(jp2Layout.createSequentialGroup()
							.addGroup(jp2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(jlSecondStepInfo0)
								.addComponent(jlSecondStepInfo1)
								.addGroup(jp2Layout.createSequentialGroup()
									.addComponent(jlCurrPrs)
									.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addComponent(jlCurrPrsVal)
									.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addComponent(jlCurrPrs1))
								.addComponent(jlSecondStepInfo2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
							.addGap(0, 0, Short.MAX_VALUE)))
					.addContainerGap())
		);
		jp2Layout.setVerticalGroup(
			jp2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jp2Layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(jlSecondStepInfo0)
					.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
					.addComponent(jbtnOpenCloseValve, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
					.addGap(18, 18, 18)
					.addComponent(jlSecondStepInfo1)
					.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
					.addGroup(jp2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(jp2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
							.addComponent(jbtnEnPump, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
							.addComponent(jbtnOpenCloseValveQuick, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGroup(jp2Layout.createSequentialGroup()
							.addComponent(jlAirPumpSetSpeed)
							.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
							.addComponent(jSliderPumpSpeed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
					.addGap(18, 18, 18)
					.addGroup(jp2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
						.addComponent(jlCurrPrs)
						.addComponent(jlCurrPrs1)
						.addComponent(jlCurrPrsVal))
					.addGap(18, 18, 18)
					.addComponent(jlSecondStepInfo2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
					.addComponent(jbtnSetFullScaleCalibr)
					.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);

		jpLog.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Журнал", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N

		jtaLogVal.setColumns(20);
		jtaLogVal.setRows(5);
		caretLog.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		jscpLog.setViewportView(jtaLogVal);

		javax.swing.GroupLayout jpLogLayout = new javax.swing.GroupLayout(jpLog);
		jpLog.setLayout(jpLogLayout);
		jpLogLayout.setHorizontalGroup(
			jpLogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jpLogLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(jscpLog)
					.addContainerGap())
		);
		jpLogLayout.setVerticalGroup(
			jpLogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jpLogLayout.createSequentialGroup()
					.addComponent(jscpLog, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
					.addGap(0, 12, Short.MAX_VALUE))
		);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(
			layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
						.addComponent(jp2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(jp1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(jpLog, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(jp1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
					.addGap(18, 18, 18)
					.addComponent(jp2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
					.addGap(18, 18, 18)
					.addComponent(jpLog, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
					.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);

		pack();
	}


	private void setListeners() {
		// This methods automatic calls when open or close this frame (window)
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				super.windowOpened(e);
				controller.calibrModeMakeEnter();
				log("Window opened", true);
			}

			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				controller.calibrModeMakeExit();
				((UI)parentFrame).registerControllerAgain();
				log("Window closing", true);
				setVisible(false);
				dispose();
			}
		});

		jbtnOpenCloseValve.setName(String.valueOf(KEY_OPEN_CLOSE_VALVE));
		jbtnEnPump.setName(String.valueOf(KEY_OPEN_CLOSE_PUMP));
		jbtnOpenCloseValveQuick.setName(String.valueOf(KEY_OPEN_CLOSE_VALVE_QUICK));

		jbtnOpenCloseValve.addMouseListener(new cmMouseAdapter());
		jbtnEnPump.addMouseListener(new cmMouseAdapter());
		jbtnOpenCloseValveQuick.addMouseListener(new cmMouseAdapter());

		jbtnOpenCloseValve.addKeyListener(new cmKeyListener());    // FIXME: 15.01.19 This listeners work only if a first pressed mouse clicked
		jbtnEnPump.addKeyListener(new cmKeyListener());
		jbtnOpenCloseValveQuick.addKeyListener(new cmKeyListener());

		jbtnSetZeroScaleCalibr.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				super.mouseClicked(e);
				log("Make calibration step 1", true);
				controller.calibrModeSetOffset();
			}
		});

		jbtnSetFullScaleCalibr.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				super.mouseClicked(e);
				log("Make calibration step 2", true);
				controller.calibrModeSetFullScale();
			}
		});

		jSliderPumpSpeed.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				super.mouseReleased(e);
				int percent = ((JSlider)e.getSource()).getValue();
				int steps = Utils.convertPercentageToSteps(155);
				controller.setSpeed(steps);
				log("speed: percent=" + percent + " | " + "dig pot steps=" + steps, true);
			}
		});
	}

	private class cmMouseAdapter extends MouseAdapter {
		@Override
		public void mousePressed(MouseEvent e) {
			super.mousePressed(e);
			mouseHandler(e, KeyEvent.KEY_PRESSED);
//			log("mousePressed");

		}

		@Override
		public void mouseReleased(MouseEvent e) {
			super.mouseReleased(e);
			mouseHandler(e, KeyEvent.KEY_RELEASED);
//			log("mouseReleased");
		}

		/**
		 * Handle msg for pressed mouse on buttons.
		 *
		 * @param me
		 */
		private void mouseHandler(MouseEvent me, int ke) {
			if (me.getSource() instanceof JButton) {
//				log("mouse clicked in button");
//				log("keyCode = " + keyCode);
				String keyCode = ((JButton) me.getSource()).getName();
				if (Utils.isNumeric(keyCode)) handler(Integer.parseInt(keyCode), ke);
				else throw new NumberFormatException("In mouseEvent keyCode variable is not numeric !");

			}
		}
	}

	private class cmKeyListener extends KeyAdapter {
		@Override
		public void keyPressed(KeyEvent e) {
			handler(e.getKeyCode(), KeyEvent.KEY_PRESSED);
		}

		@Override
		public void keyReleased(KeyEvent e) {
			handler(e.getKeyCode(), KeyEvent.KEY_RELEASED);
		}
	}


	/**
	 * Handles pressed or released any key on keyboard
	 *
	 * @param keyCode integer code associated with pressed key in this event
	 * @param e key event only precessed KEY_PRESSED or KEY_RELEASED in {@link KeyEvent}
	 */
	private void handler(int keyCode, int e) {
		if (e == KeyEvent.KEY_PRESSED) {
			switch (keyCode) {
				case KEY_OPEN_CLOSE_VALVE:
					log("Valve open/close | KEY_PRESSED | hotKey = " + KeyEvent.getKeyText(KEY_OPEN_CLOSE_VALVE), true);
					controller.calibrModeOpenCloseValve();
					break;
				case KEY_OPEN_CLOSE_PUMP:
					log("Pump en/dis | KEY_PRESSED | hotKey = " + KeyEvent.getKeyText(KEY_OPEN_CLOSE_PUMP), true);
					controller.calibrModeOpenClosePump();
					break;
				case KEY_OPEN_CLOSE_VALVE_QUICK:
					log("Valve quick open/close | KEY_PRESSED | hotKey = " + KeyEvent.getKeyText(KEY_OPEN_CLOSE_VALVE_QUICK), true);
					controller.calibrModeOpenCloseValveQuick();
					break;
			}
		} else if (e == KeyEvent.KEY_RELEASED){
			switch (keyCode) {
				case KEY_OPEN_CLOSE_VALVE:
//					// Not used
					break;
				case KEY_OPEN_CLOSE_PUMP:
					log("Pump en/dis | KEY_RELEASED | hotKey = " + KeyEvent.getKeyText(KEY_OPEN_CLOSE_PUMP), true);
					controller.calibrModeOpenClosePump();
					break;
				case KEY_OPEN_CLOSE_VALVE_QUICK:
					// Not used
					break;
			}
		}
	}


	@Override
	public void logStatus(String msg) {
		log("Set log status msg in this UI = " + msg, true);
		msg += "\n";
		String currText = jtaLogVal.getText();
		jtaLogVal.setText(currText + msg);
	}

	@Override
	public void setCurrPrs(int prs) {
		log("curr pressure = " + prs, true);
		jlCurrPrsVal.setText(String.valueOf(prs));
	}

	/**
	 * Logging message for debug this class.
	 * Messages send to stdout.
	 * Enable and disable this log can be done using {@link #isLog}.
	 *
	 * @param msg message text displaying in log
	 * @param newLine <tt>true</tt> if add new line in end of message
	 */
	private void log(String msg, boolean newLine){
		msg = UICalibrMonitor.class.getSimpleName() + " # " + msg + ((newLine) ? "\n" : "");
		if (isLog) System.out.print(msg);
	}
}
