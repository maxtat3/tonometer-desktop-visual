package ui;

import model.UART;
import util.AppProperties;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;


/**
 * UI dialog window to show and tune setting/options of this app.
 */
public class UISettingsDialog extends JDialog {

	private static final boolean isLog = true;

	public static final char MNEMONIC_KEY_COM_PORTS = 'c';

	private JFrame parentFrame;
	private JCheckBox jchbAutoConn;
	private final JComboBox<String> jcmbPort = new JComboBox<String>();
	private String portBefore, portAfter;
	private boolean portChanged;

	private boolean okClicked = false;


	public UISettingsDialog(JFrame frame) {    // TODO: 16.02.19 may be separate to buildUI and controller
		super(frame, "Настройки", true);

		Container container = getContentPane();

		// Create a pane with an empty border for spacing
		Border emptyBorder = BorderFactory.createEmptyBorder(2, 5, 5, 5);
		JPanel emptyBorderPanel = new JPanel();
		emptyBorderPanel.setLayout(new BoxLayout(emptyBorderPanel, BoxLayout.Y_AXIS));
		emptyBorderPanel.setBorder(emptyBorder);
		container.add(emptyBorderPanel);


		// Create general etched border
		Border etchedBorder = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);

		// *************************************
		// 		COM port connection section
		// *************************************
		// Create a pane with an title etched border
		Border etchedBorderPortConn = BorderFactory.createTitledBorder(etchedBorder, "Подключение к устройству");
		JPanel portPanel = new JPanel(new GridBagLayout());
		portPanel.setBorder(etchedBorderPortConn);
		emptyBorderPanel.add(portPanel);

		GridBagConstraints c = new GridBagConstraints();

		// COM port label row
		JLabel localeLabel = new JLabel("COM порт:");
		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.LINE_START;
		c.insets = new Insets(0, 5, 3, 0);
		c.weightx = 1;
		c.weighty = 0;
		c.gridwidth = 2;
		c.fill = GridBagConstraints.NONE;
		portPanel.add(localeLabel, c);

		// Remember the open port at the time of opening this dialog
		portBefore = AppProperties.get(AppProperties.Options.CONN_PORT_NAME, UART.DefaultCOMPort.NAME);
		if (!matchPortName(portBefore)) portBefore = UART.DefaultCOMPort.NAME;
		log("portBefore = " + portBefore);
		// Select COM port Combo box row
		jcmbPort.setModel(new DefaultComboBoxModel<String>(UART.NUMBERS_OF_COM_PORTS));
		jcmbPort.setSelectedItem(portBefore);
		jcmbPort.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				portAfter = jcmbPort.getSelectedItem().toString();
			}
		});

		// Set mnemonic key for select COM port Combo box. For it's use press Alt + mnemonic key
		JLabel jlCOMPorts = new JLabel("COM ports");	// not displayed in UI
		jlCOMPorts.setDisplayedMnemonic(MNEMONIC_KEY_COM_PORTS);
		jlCOMPorts.setLabelFor(jcmbPort);

		c.gridx = 0;
		c.gridy = 1;
		c.anchor = GridBagConstraints.LINE_START;
		c.insets = new Insets(0, 5, 8, 5);
		c.weightx = 1;
		c.weighty = 0;
		c.gridwidth = 2;
		c.fill = GridBagConstraints.HORIZONTAL;
		portPanel.add(jcmbPort, c);


		// Auto connection check box row
		jchbAutoConn = new JCheckBox("Автоподключение");
		c.gridx = 0;
		c.gridy = 2;
		c.anchor = GridBagConstraints.LINE_START;
		c.insets = new Insets(0, 2, 5, 0);
		c.weightx = 1;
		c.weighty = 0;
		c.gridwidth = 1;
		c.fill = GridBagConstraints.NONE;
		portPanel.add(jchbAutoConn, c);
		jchbAutoConn.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				jcmbPort.setEnabled(e.getStateChange() == ItemEvent.DESELECTED);

			}
		});
		boolean isAutoConn = Boolean.parseBoolean(AppProperties.get(AppProperties.Options.CONN_TO_SERVER_AUTO, AppProperties.Options.FALSE));
		jchbAutoConn.setSelected(isAutoConn);


		// Some spacing
		emptyBorderPanel.add(Box.createVerticalGlue());


		// ***************************************
		// *	The Ok and Cancel buttons row	 *
		// ***************************************
		JPanel btnPanel = new JPanel(new FlowLayout());
		emptyBorderPanel.add(btnPanel);
		JButton okBtn = new JButton("Ok");
		okBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButtonAction();
			}
		});
		btnPanel.add(okBtn);

		JButton cancelBtn = new JButton("Cancel");
		cancelBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
			}
		});
		btnPanel.add(cancelBtn);
	}

	private void okButtonAction() {
		try {
			AppProperties.set(AppProperties.Options.CONN_TO_SERVER_AUTO, String.valueOf(jchbAutoConn.isSelected()));

			log("portBefore="+portBefore);
			log("portAfter="+portAfter);

			if (portAfter != null && !portAfter.equals(portBefore)) {
				log("port changed and saved");
				portChanged = true;
				AppProperties.set(AppProperties.Options.CONN_PORT_NAME, portAfter);
			} else {
				log("Set default port !!!");
			}

			AppProperties.save();
			setVisible(false);
			dispose();
			okClicked = true;
		} catch (IOException e) {
			JOptionPane.showMessageDialog(parentFrame, e.getStackTrace(),"Ошибка записи в файл конфигураци", JOptionPane.ERROR_MESSAGE);
		}
	}

	public boolean isOkClicked() {
		return okClicked;
	}

	public boolean hasPortChanged() {
		return portChanged;
	}

	private boolean matchPortName(String portName) {
		boolean match = false;
		for(String port : UART.NUMBERS_OF_COM_PORTS) {
			if (portName.equals(port)) match = true;
		}
		return match;
	}


	private void log(String msg) {
		if (isLog) System.out.println(UISettingsDialog.class.getSimpleName() + ": " + msg);
	}

}
