package ui;

import model.UART;

/**
 *
 */
public interface UICallback {

	/**
	 * Set COM port state in UI.
	 * List port states in {@link UART.PortStates}
	 * This set COM port state in UI label.
	 *
	 * @param portStates available UART port states lists
	 *        in {@link UART.PortStates}
	 */
	void setPortState(UART.PortStates portStates);

	/**
	 * Add channel 1 point data to chart.
	 *
	 * @param val channel 1 point data
	 * @param extraMs
	 */
	void addPoints(int val, long extraMs);

	/**
	 * Show current natural pressure in measuring process.
	 *
	 * @param val natural pressure in mmHg units.
	 */
	void showPressure(int val);

	/**
	 * Show resulted diastolic and systolic pressure after end measure process.
	 *
	 * @param dia diastolic pressure in mmHg units.
	 * @param sys systolic pressure in mmHg units.
	 */
	void showDiaSys(double dia, double sys);

	/**
	 * Show elapsed time started process.
	 *
	 * @param val formatted time in minute:second
	 *
	 * @see {@link controller.Controller#TIME_FORMAT_PATTERN}
	 */
	void showElapsedMsrTime(String val);

	/**
	 * Show remaining battery charge in device
	 *
	 * @param val charge in percents.
	 */
	void showBattCharge(int val);

	/**
	 * Reset all data points in chart.
	 */
	void resetChartData();
}
