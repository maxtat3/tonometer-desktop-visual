package ui;

import controller.ControllerSensorSelect;
import model.UART;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


/**
 * Окно выбора датчика давления.
 * Только для тестовых целей.
 */
public class UISensorSelect extends JDialog{

	private static final boolean isLog = true;
	public static final String TXT_MPX_2053 = "MPX2053";
	public static final String TXT_24_PCB_FA_6G = "24PCBFA6G";

	private JToggleButton jtbtnMpx2053, jtbtn24pcbfa6g;

	private JFrame parentFrame;
	private final ControllerSensorSelect controller;


	public UISensorSelect(JFrame parentFrame, UART uart) {
		super(parentFrame);
		this.parentFrame = parentFrame;

		if (parentFrame != null) {
			Dimension parentSize = parentFrame.getSize();
			Point p = parentFrame.getLocation();
			setLocation(p.x + parentSize.width / 4, p.y + parentSize.height / 4);
		}

		controller = new ControllerSensorSelect(uart);

		buildUI();
		pack();
		setListeners();
		setVisible(true);
	}

	private void buildUI() {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Выбор датчика давления");
		setResizable(false);

		jtbtnMpx2053 = new JToggleButton(TXT_MPX_2053);
		jtbtn24pcbfa6g = new JToggleButton(TXT_24_PCB_FA_6G);

		JPanel jpRoot = new JPanel();
		jpRoot.setLayout(new FlowLayout(FlowLayout.CENTER));

		jpRoot.add(jtbtnMpx2053);
		jpRoot.add(jtbtn24pcbfa6g);

		add(jpRoot);
	}

	private void setListeners() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				super.windowOpened(e);
				log("Window opened", true);
			}

			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				((UI)parentFrame).registerControllerAgain();
				log("Window closing", true);
				setVisible(false);
				dispose();
			}
		});

		jtbtnMpx2053.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.selectMpx2053();
				jtbtn24pcbfa6g.setSelected(false);
			}
		});

		jtbtn24pcbfa6g.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.select24pcbfa6g();
				jtbtnMpx2053.setSelected(false);
			}
		});
	}

	/**
	 * Logging message for debug this class.
	 * Messages send to stdout.
	 * Enable and disable this log can be done using {@link #isLog}.
	 *
	 * @param msg message text displaying in log
	 * @param newLine <tt>true</tt> if add new line in end of message
	 */
	private void log(String msg, boolean newLine){
		msg = UISensorSelect.class.getSimpleName() + " # " + msg + ((newLine) ? "\n" : "");
		if (isLog) System.out.print(msg);
	}
}
