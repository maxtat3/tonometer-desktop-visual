package ui;

/**
 *
 */
public interface UICalibrMonitorCallback {

	/**
	 * Allow set status in log after make step 1 and step 2 of calibration process.
	 *
	 * @param msg state written in log
	 */
	void logStatus(String msg);

	/**
	 * Set current DC pressure in pneumatic system.
	 *
	 * @param prs current DC pressure
	 */
	void setCurrPrs(int prs);

}
