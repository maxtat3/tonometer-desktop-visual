package ui;

import controller.Controller;
import model.UART;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
import util.AppProperties;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Ellipse2D;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 */
public class UI extends ApplicationFrame implements UICallback {

	private static final boolean IS_LOG = false;

//	private static final String TITLE_CHART_TXT = "App for test received buffered data from UART";
	private static final String TITLE_CHART_TXT = "";
	private static final String PORT_CLOSE_TXT = "Не найдено";
	private static final String PORT_OPEN_TXT = "Подключено";
	private static final String AXIS_X_NAME_TXT = "Time";
	private static final String AXIS_Y_NAME_TXT = "ADC 24-bit value";
	private static final double MIN_AXIS_VALUE = -15000;
	private static final double MAX_AXIS_VALUE = 75000;

	private static final String[] speed = {
		"10", "20", "30", "40", "50",
		"60", "70", "80", "90", "100"
	};
	private static final String[] stopPrs = {
		"120", "130", "140", "150",
		"170", "190", "210", "230", "250",
	};


	public static final char MNEMONIC_KEY_BTN_START_STOP = 's';

	private TimeSeries series1;
	private long initTimeMs;	// time from which countdown begin
//	private long initTimeMs = System.currentTimeMillis();	// ... or start from current time

	private JFrame rootFrame = this;

	// View components

	private JLabel lbPortState;
	private JButton btnStartProcess;
	private JButton jBtnSettings;
	private JComboBox<String> jcmbxPumpSpeed;
	private JComboBox<String> jcmbxStopPrs;
	private JLabel jlCurrPrs;
	private JLabel jlDiaPrs;
	private JLabel jlSysPrs;
	private JLabel jlMsrTime;
	private JLabel jlBattCharge;

	private static Class class$org$jfree$data$time$Millisecond; /* synthetic field */
	private Controller controller;

	private UART uart;


	private static final Shape circle = new Ellipse2D.Double(-3, -3, 6, 6);
	private static final Color line = Color.gray;



	/**
	 * Create application main window.
	 *
	 * @param title title main window, placed in top
	 */
	public UI(String title){
		super(title);

		MainPanel mainPanel = new MainPanel();
		lbPortState = mainPanel.getJlPortState();
		btnStartProcess = mainPanel.getJbtnStartProcess();
		jBtnSettings = mainPanel.getJbtnSettings();

		jcmbxPumpSpeed = mainPanel.getJcmbxPumpSpeed();
		jcmbxStopPrs = mainPanel.getJcmbxStopPrs();

		jlCurrPrs = mainPanel.getJlCurrPrs();

		jlDiaPrs = mainPanel.getJlDiaPrs();
		jlSysPrs = mainPanel.getJlSysPrs();

		jlMsrTime = mainPanel.getJlMsrTime();
		jlBattCharge = mainPanel.getJlBattCharge();

		uart = new UART();
		controller = new Controller(this, uart);
		uart.addControllerCallback(controller);
		controller.connectToDevice(AppProperties.get(AppProperties.Options.CONN_PORT_NAME, UART.DefaultCOMPort.NAME));

		setContentPane(mainPanel);
		initDirPanelComponents();

		setPreferredSize(new Dimension(1024, 630));
		setMinimumSize(new Dimension(1024, 630));

		RefineryUtilities.centerFrameOnScreen(this);
		pack();
		setVisible(true);

		controller.startListenBattCharge();
	}

	@Override
	public void windowClosing(WindowEvent event) {
		if (controller.isOpenUSARTPort()) {
			controller.closeUSARTPort();
			log("UART port released");
		}
		log("Exit of app");
		if (event.getWindow() == this) {
			dispose();
			System.exit(0);
		}
	}

	/**
	 * Make register again this UI callback in model.
	 */
	public void registerControllerAgain() {
		uart.addControllerCallback(controller);
	}


	/**
	 * Main panel contained chart and directions panels
	 */
	private class MainPanel extends JPanel {

		public MainPanel(){
			super(new BorderLayout());
			JPanel dirPanel = new JPanel();
			dirPanel.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));

			registerDialogsAndFrames();

			initComponents();
			dirPanel.add(jpDirections);
			dirPanel.add(jpResults);

			add(createChartPanel());
			add(dirPanel, BorderLayout.NORTH);
		}

		private javax.swing.JButton jbtnSettings;
		private javax.swing.JButton jbtnStartProcess;
		private javax.swing.JCheckBox jchbStopPrsAuto;
		private javax.swing.JComboBox<String> jcmbxPumpSpeed;
		private javax.swing.JComboBox<String> jcmbxStopPrs;
		private javax.swing.JLabel jlAvgPrs;
		private javax.swing.JLabel jlAvgPrsInfo;
		private javax.swing.JLabel jlBattCharge;
		private javax.swing.JLabel jlBattChargeInfo;
		private javax.swing.JLabel jlCardicOut;
		private javax.swing.JLabel jlCardicOutInfo;
		private javax.swing.JLabel jlCurrPrs;
		private javax.swing.JLabel jlCurrPrsInfo0;
		private javax.swing.JLabel jlDiaPrs;
		private javax.swing.JLabel jlDiaPrsInfo;
		private javax.swing.JLabel jlHeartBeat;
		private javax.swing.JLabel jlHeartBeatInfo;
		private javax.swing.JLabel jlMsrTime;
		private javax.swing.JLabel jlMsrTimeInfo;
		private javax.swing.JLabel jlPortState;
		private javax.swing.JLabel jlPortStateInfo;
		private javax.swing.JLabel jlPumpSpeedInfo;
		private javax.swing.JLabel jlStopPrsInfo;
		private javax.swing.JLabel jlSysPrs;
		private javax.swing.JLabel jlSysPrsInfo;
		private javax.swing.JLabel jlVascCompl;
		private javax.swing.JLabel jlVascComplInfo;
		private javax.swing.JLabel jlVascResist;
		private javax.swing.JLabel jlVascResistInfo;
		private javax.swing.JPanel jpDirections;
		private javax.swing.JPanel jpResults;
		private javax.swing.JSeparator jsp1;
		private javax.swing.JSeparator jsp2;
		private javax.swing.JSeparator jsp3;
		private javax.swing.JSeparator jsp4;

		private void initComponents() {
			jpDirections = new javax.swing.JPanel();
			jlPortStateInfo = new javax.swing.JLabel();
			jlPortState = new javax.swing.JLabel();
			jbtnStartProcess = new javax.swing.JButton();
			jlPumpSpeedInfo = new javax.swing.JLabel();
			jlStopPrsInfo = new javax.swing.JLabel();
			jcmbxPumpSpeed = new javax.swing.JComboBox<>();
			jcmbxStopPrs = new javax.swing.JComboBox<>();
			jchbStopPrsAuto = new javax.swing.JCheckBox();
			jpResults = new javax.swing.JPanel();
			jsp1 = new javax.swing.JSeparator();
			jsp2 = new javax.swing.JSeparator();
			jsp3 = new javax.swing.JSeparator();
			jsp4 = new javax.swing.JSeparator();
			jlCurrPrsInfo0 = new javax.swing.JLabel();
			jlCurrPrs = new javax.swing.JLabel();
			jlSysPrsInfo = new javax.swing.JLabel();
			jlSysPrs = new javax.swing.JLabel();
			jlDiaPrsInfo = new javax.swing.JLabel();
			jlDiaPrs = new javax.swing.JLabel();
			jlAvgPrsInfo = new javax.swing.JLabel();
			jlAvgPrs = new javax.swing.JLabel();
			jlHeartBeatInfo = new javax.swing.JLabel();
			jlHeartBeat = new javax.swing.JLabel();
			jlMsrTimeInfo = new javax.swing.JLabel();
			jlMsrTime = new javax.swing.JLabel();
			jlBattChargeInfo = new javax.swing.JLabel();
			jlBattCharge = new javax.swing.JLabel();
			jbtnSettings = new javax.swing.JButton();
			jlCardicOutInfo = new javax.swing.JLabel();
			jlCardicOut = new javax.swing.JLabel();
			jlVascResistInfo = new javax.swing.JLabel();
			jlVascResist = new javax.swing.JLabel();
			jlVascComplInfo = new javax.swing.JLabel();
			jlVascCompl = new javax.swing.JLabel();

//			setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

			/* Direction panel */
			jpDirections.setBorder(javax.swing.BorderFactory.createEtchedBorder());

			jlPortStateInfo.setText("Устройство:");
			jbtnStartProcess.setText("Старт / стоп");
			jlPumpSpeedInfo.setText("Скорость нагнетания (%):");
			jlStopPrsInfo.setText("<html>Давление остановки<br>(мм.рт.ст.):</html>");
			jchbStopPrsAuto.setText("Авто");

			javax.swing.GroupLayout jpDirectionsLayout = new javax.swing.GroupLayout(jpDirections);
			jpDirections.setLayout(jpDirectionsLayout);
			jpDirectionsLayout.setHorizontalGroup(
				jpDirectionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(jpDirectionsLayout.createSequentialGroup()
						.addGroup(jpDirectionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
							.addGroup(jpDirectionsLayout.createSequentialGroup()
								.addGap(51, 51, 51)
								.addComponent(jcmbxPumpSpeed, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
							.addGroup(jpDirectionsLayout.createSequentialGroup()
								.addContainerGap()
								.addComponent(jlStopPrsInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
							.addGroup(jpDirectionsLayout.createSequentialGroup()
								.addContainerGap()
								.addGroup(jpDirectionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
									.addGroup(jpDirectionsLayout.createSequentialGroup()
										.addComponent(jlPortStateInfo)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(jlPortState))
									.addGroup(jpDirectionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
										.addComponent(jbtnStartProcess, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addGroup(javax.swing.GroupLayout.Alignment.LEADING, jpDirectionsLayout.createSequentialGroup()
											.addComponent(jcmbxStopPrs, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
											.addComponent(jchbStopPrsAuto))
										.addComponent(jlPumpSpeedInfo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
			);
			jpDirectionsLayout.setVerticalGroup(
				jpDirectionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(jpDirectionsLayout.createSequentialGroup()
						.addContainerGap()
						.addGroup(jpDirectionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
							.addComponent(jlPortStateInfo)
							.addComponent(jlPortState))
						.addGap(18, 18, 18)
						.addComponent(jbtnStartProcess)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(jlPumpSpeedInfo)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(jcmbxPumpSpeed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(jlStopPrsInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(jpDirectionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
							.addComponent(jcmbxStopPrs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
							.addComponent(jchbStopPrsAuto))
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
			);

			jlStopPrsInfo.getAccessibleContext().setAccessibleName("");

			/* Results panel */
			jpResults.setBorder(javax.swing.BorderFactory.createEtchedBorder());

			jsp1.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(0, 0, 0)));
			jsp2.setOrientation(javax.swing.SwingConstants.VERTICAL);
			jsp3.setOrientation(javax.swing.SwingConstants.VERTICAL);
			jsp4.setOrientation(javax.swing.SwingConstants.VERTICAL);

			jlCurrPrsInfo0.setText("Давление в системе (мм.рт.ст.):");
			jlCurrPrs.setText("---");

			jlSysPrsInfo.setText("Верхнее давление (мм.рт.ст.):");
			jlSysPrs.setText("---");

			jlDiaPrsInfo.setText("Нижнее давление (мм.рт.ст.):");
			jlDiaPrs.setText("---");

			jlAvgPrsInfo.setText("Среднее давление (мм.рт.ст.):");
			jlAvgPrs.setText("---");

			jlHeartBeatInfo.setText("Пульс (уд/с):");
			jlHeartBeat.setText("---");

			jlMsrTimeInfo.setText("Время измерения:");
			jlMsrTime.setText("--:--");

			jlBattChargeInfo.setText("Заряд АКБ (%):");
			jlBattCharge.setText("---");

			jbtnSettings.setText("Настройки");

			jlCardicOutInfo.setText("Cardic output:");
			jlCardicOut.setText("---");

			jlVascResistInfo.setText("Vascular resist:");
			jlVascResist.setText("---");

			jlVascComplInfo.setText("Vascular compliance:");
			jlVascCompl.setText("---");

			javax.swing.GroupLayout jpResultsLayout = new javax.swing.GroupLayout(jpResults);
			jpResults.setLayout(jpResultsLayout);
			jpResultsLayout.setHorizontalGroup(
				jpResultsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(jpResultsLayout.createSequentialGroup()
						.addContainerGap()
						.addGroup(jpResultsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
							.addComponent(jsp1)
							.addGroup(jpResultsLayout.createSequentialGroup()
								.addComponent(jlHeartBeatInfo)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(jlHeartBeat))
							.addGroup(jpResultsLayout.createSequentialGroup()
								.addGroup(jpResultsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
									.addGroup(jpResultsLayout.createSequentialGroup()
										.addComponent(jlSysPrsInfo)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(jlSysPrs))
									.addGroup(jpResultsLayout.createSequentialGroup()
										.addComponent(jlDiaPrsInfo)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(jlDiaPrs))
									.addGroup(jpResultsLayout.createSequentialGroup()
										.addComponent(jlAvgPrsInfo)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(jlAvgPrs)))
								.addGap(18, 18, 18)
								.addGroup(jpResultsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
									.addGroup(jpResultsLayout.createSequentialGroup()
										.addComponent(jlVascComplInfo)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(jlVascCompl))
									.addGroup(jpResultsLayout.createSequentialGroup()
										.addComponent(jlVascResistInfo)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(jlVascResist))
									.addGroup(jpResultsLayout.createSequentialGroup()
										.addComponent(jlCardicOutInfo)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(jlCardicOut))))
							.addGroup(jpResultsLayout.createSequentialGroup()
								.addComponent(jlCurrPrsInfo0)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(jlCurrPrs)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(jsp2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(jlMsrTimeInfo)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(jlMsrTime)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(jsp3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(jlBattChargeInfo)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(jlBattCharge)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(jsp4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(jbtnSettings)))
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
			);
			jpResultsLayout.setVerticalGroup(
				jpResultsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(jpResultsLayout.createSequentialGroup()
						.addContainerGap()
						.addGroup(jpResultsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
							.addGroup(jpResultsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
								.addComponent(jlCurrPrs)
								.addComponent(jsp2, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addGroup(jpResultsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
									.addComponent(jlMsrTimeInfo)
									.addComponent(jlMsrTime))
								.addComponent(jsp3, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addGroup(jpResultsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
									.addComponent(jlBattChargeInfo)
									.addComponent(jlBattCharge))
								.addComponent(jsp4, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(jbtnSettings))
							.addComponent(jlCurrPrsInfo0, javax.swing.GroupLayout.Alignment.TRAILING))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(jsp1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jpResultsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
							.addGroup(jpResultsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(jlSysPrs)
								.addComponent(jlCardicOutInfo)
								.addComponent(jlCardicOut))
							.addComponent(jlSysPrsInfo))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jpResultsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
							.addComponent(jlDiaPrsInfo)
							.addComponent(jlDiaPrs)
							.addComponent(jlVascResistInfo)
							.addComponent(jlVascResist))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jpResultsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
							.addComponent(jlAvgPrsInfo)
							.addComponent(jlAvgPrs)
							.addComponent(jlVascComplInfo)
							.addComponent(jlVascCompl))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jpResultsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
							.addComponent(jlHeartBeatInfo)
							.addComponent(jlHeartBeat))
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
			);

			javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
			getContentPane().setLayout(layout);
			layout.setHorizontalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(layout.createSequentialGroup()
						.addContainerGap()
						.addComponent(jpDirections, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(jpResults, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
			);
			layout.setVerticalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
							.addComponent(jpDirections, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(jpResults, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
			);
		}

		private ChartPanel createChartPanel() {
			ChartPanel chartpanel = new ChartPanel(createChart());
			// добавление линий курсора
			chartpanel.setHorizontalAxisTrace(true);
			chartpanel.setVerticalAxisTrace(true);
//			chartpanel.setMouseZoomable(true, false);
			return chartpanel;
		}

		/**
		 * This dialogs or frames should be called from hot keys from root (this) frame.
		 */
		private void registerDialogsAndFrames(){
			ActionListener cml = new ActionListener() {
				public void actionPerformed(ActionEvent actionEvent) {
					System.out.println("Open calibration monitor modal frame");
					new UICalibrMonitor(rootFrame, uart);
				}
			};
			KeyStroke keystroke = KeyStroke.getKeyStroke(KeyEvent.VK_K, InputEvent.CTRL_MASK + InputEvent.ALT_MASK + InputEvent.SHIFT_MASK, false);
			registerKeyboardAction(cml, keystroke, JComponent.WHEN_IN_FOCUSED_WINDOW);

			ActionListener psensl = new ActionListener() {
				public void actionPerformed(ActionEvent actionEvent) {
					System.out.println("Open select pressure sensor modal frame");
					new UISensorSelect(rootFrame, uart);
				}
			};
			KeyStroke psenslKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK + InputEvent.ALT_MASK + InputEvent.SHIFT_MASK, false);
			registerKeyboardAction(psensl, psenslKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);

			// Open options window
			ActionListener optl = new ActionListener() {
				public void actionPerformed(ActionEvent actionEvent) {
					showOptionsDialog();
				}
			};
			KeyStroke optionsKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_MASK, false);
			registerKeyboardAction(optl, optionsKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);

		}

		private JComponent createVerticalSeparator() {
			JSeparator x = new JSeparator(SwingConstants.VERTICAL);
			x.setPreferredSize(new Dimension(1, 24));
			return x;
		}


		/**
		 * Create chart from 4 channels times series collection
		 *
		 * @return building chart
		 * @see TimeSeriesCollection
		 */
		private JFreeChart createChart(){
			series1 = new TimeSeries("", UI.class$org$jfree$data$time$Millisecond != null ? UI.class$org$jfree$data$time$Millisecond : (UI.class$org$jfree$data$time$Millisecond = UI.class$("org.jfree.data.time.Millisecond")));

			initTimeXAxisInChart();

			TimeSeriesCollection xyCh1 = new TimeSeriesCollection(series1);

			JFreeChart jfreechart = ChartFactory.createTimeSeriesChart(
				TITLE_CHART_TXT,
				AXIS_X_NAME_TXT,
				AXIS_Y_NAME_TXT,
				xyCh1,
				false,
				true,
				false
			);


			// easy variant 0
			XYPlot xyplot = (XYPlot)jfreechart.getPlot();
			XYItemRenderer renderer = xyplot.getRenderer();
			renderer.setSeriesPaint(0, Color.BLUE);

			// advanced variant 1
//			XYPlot xyplot = (XYPlot)jfreechart.getPlot();
//
//			ShapeRenderer renderer = new ShapeRenderer(true, true, 1000000);
//			xyplot.setRenderer(renderer);
//			renderer.setSeriesShape(0, circle);
//			renderer.setSeriesPaint(0, line);
//			renderer.setUseFillPaint(true);		// показывать ли заливку внутри маркеров ? true  - цветная заливка, false - заливке берется от цвета линии
//			renderer.setSeriesShapesFilled(0, true);	// показывать ли заливку в середине маркера ? true - да , false - прозрачный цвет
//			renderer.setSeriesShapesVisible(0, true);	// показывать маркеры на линии ? true - да, false - нет
//			renderer.setUseOutlinePaint(true);		// используется ли контурная краска для рисования контуров фигуры
//			renderer.setSeriesOutlinePaint(0, line);
//			ValueAxis range = xyplot.getRangeAxis();
//			range.setLowerBound(0.5);
//
			ValueAxis va = xyplot.getDomainAxis();
			va.setAutoRange(true);
//			va.setFixedAutoRange(60000D);
			va = xyplot.getRangeAxis();
			va.setRange(MIN_AXIS_VALUE, MAX_AXIS_VALUE);

			return jfreechart;
		}

		public JLabel getJlPortState() {
			return jlPortState;
		}

		public JButton getJbtnStartProcess() {
			return jbtnStartProcess;
		}

		public JComboBox<String> getJcmbxPumpSpeed() {
			return jcmbxPumpSpeed;
		}

		public JComboBox<String> getJcmbxStopPrs() {
			return jcmbxStopPrs;
		}

		public JLabel getJlAvgPrs() {
			return jlAvgPrs;
		}

		public JLabel getJlMsrTime() {
			return jlMsrTime;
		}

		public JLabel getJlBattCharge() {
			return jlBattCharge;
		}

		public JLabel getJlCurrPrs() {
			return jlCurrPrs;
		}

		public JLabel getJlDiaPrs() {
			return jlDiaPrs;
		}

		public JLabel getJlHeartBeat() {
			return jlHeartBeat;
		}

		public JLabel getJlSysPrs() {
			return jlSysPrs;
		}

		public JButton getJbtnSettings() {
			return jbtnSettings;
		}
	}

	private void initTimeXAxisInChart() {
		String initTimeStr = "00:00:00.000";
		SimpleDateFormat sdf = new SimpleDateFormat("HH:MM:SS.SSS");
		try {
			Date parsedDate = sdf.parse(initTimeStr);
			initTimeMs = parsedDate.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	private static class ShapeRenderer extends XYLineAndShapeRenderer {

		private java.util.List<Color> clut;

		public ShapeRenderer(boolean lines, boolean shapes, int n) {
			super(lines, shapes);
			clut = new ArrayList<Color>(n);
			for (int i = 0; i < n; i++) {
				clut.add(Color.getHSBColor((float) i / n, 1, 1));
			}
		}

		@Override
		public Paint getItemFillPaint(int row, int column) {
			return clut.get(column);
		}
	}

	/**
	 * Initialization and settings view components placed in direction panel.
	 */
	private void initDirPanelComponents() {
		btnStartProcess.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.doStartStopMsr();
			}
		});
		// Added mnemonics (hot keys) to UI elements. For it's use press Alt + mnemonic key
		btnStartProcess.setMnemonic(MNEMONIC_KEY_BTN_START_STOP);

		jBtnSettings.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				showOptionsDialog();
			}
		});

		/* Pump speed setting */
		// TODO: 29.03.19 - check of all elements of arrau speed contained int numbers
		jcmbxPumpSpeed.setModel(new javax.swing.DefaultComboBoxModel<>(speed));
		int defSpeedIndex = (int) Math.floor(speed.length / 2) - 1;	// default value - set half of speed range
		jcmbxPumpSpeed.setSelectedIndex(defSpeedIndex);
		controller.setSpeed((int) ((Math.pow(2, 6) / 100) * Integer.parseInt(speed[defSpeedIndex])) );
		jcmbxPumpSpeed.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO: 29.03.19 See APIToMCU # convertPercentageToSteps method
				int percentage = Integer.parseInt(((String) jcmbxPumpSpeed.getSelectedItem()));
				int res = (int) ((Math.pow(2, 6) / 100) * percentage);
				controller.setSpeed(res);
			}
		});

		jcmbxStopPrs.setModel(new javax.swing.DefaultComboBoxModel<>(stopPrs));
		jcmbxStopPrs.setSelectedIndex(3);
	}

	private void showOptionsDialog() {
		log("Open Options modal frame");
		UISettingsDialog uiSettingsDialog = new UISettingsDialog(rootFrame);
		uiSettingsDialog.pack();
		uiSettingsDialog.setLocationRelativeTo(rootFrame);
		uiSettingsDialog.setVisible(true);

		if (uiSettingsDialog.isOkClicked()) {
			log("ok click");
			boolean autoConn = Boolean.parseBoolean(AppProperties.get(AppProperties.Options.CONN_TO_SERVER_AUTO, AppProperties.Options.FALSE));
			if (autoConn) {
				// do make auto-connection

			} else if (uiSettingsDialog.hasPortChanged()) {
				log("Try create new connection by received port");
				String port = AppProperties.get(AppProperties.Options.CONN_PORT_NAME, UART.DefaultCOMPort.NAME);
				controller.connectToDevice(port);
			}
		}
	}

	static Class class$(String s){
		Class clazz = null;
		try {
			clazz= Class.forName(s);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return clazz;
	}

	@Override
	public void setPortState(UART.PortStates portStates) {
		if (portStates == UART.PortStates.OPEN) {
			lbPortState.setText(PORT_OPEN_TXT);
			lbPortState.setForeground(new Color(50, 205, 50));

		} else if (portStates == UART.PortStates.CLOSE) {
			lbPortState.setText(PORT_CLOSE_TXT);
			lbPortState.setForeground(Color.red);
		}
	}


//	long l = System.currentTimeMillis();

	@Override
	public void addPoints(int val, long extraMs) {
//		log("@ addPoints");
//		log("extraMs = " + extraMs);

//		l = l + extraMs;
//		Millisecond ms = new Millisecond(new Date(l));
//		series1.addOrUpdate(ms, val);

		initTimeMs = initTimeMs + extraMs;
		Millisecond ms = new Millisecond(new Date(initTimeMs));
		series1.addOrUpdate(ms, val);
	}

	@Override
	public void showPressure(int val) {
		jlCurrPrs.setText(String.valueOf(val));
	}

	@Override
	public void showDiaSys(double dia, double sys) {
//		jlDiaPrs.setText(String.format("%.0f", dia));
//		jlSysPrs.setText(String.format("%.0f", sys));

		jlDiaPrs.setText( String.valueOf(Math.round(dia)) );
		jlSysPrs.setText(String.valueOf(Math.round(sys)) );
	}

	@Override
	public void showElapsedMsrTime(String val) {
		jlMsrTime.setText(val);
	}

	@Override
	public void showBattCharge(int val) {
		jlBattCharge.setText(String.valueOf(val));
	}

	@Override
	public void resetChartData() {
		initTimeXAxisInChart();
		series1.clear();
	}

	private void log(String msg) {
		if (IS_LOG) System.out.println(UI.class.getSimpleName() + ": " + msg);
	}

}
